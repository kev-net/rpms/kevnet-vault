vault {
  address = "https://vault.int.kevnet.net:8200"
  ca_cert = "/usr/share/pki/ca-trust-source/anchors/kevnet-ca.pem"
}

cache {
}
