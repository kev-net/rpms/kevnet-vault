auto_auth {
  method "aws" {
    config = {
      type = "iam"
      role = "ec2-role"
      header_value = "vault.int.kevnet.net"
    }
  }

  sink "file" {
    config = {
      path = "/run/vault-agent-token"
      mode = 0600
    }
  }
}
