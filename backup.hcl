template {
  contents = "{{ with secret \"secret/data/backup_keys\" }}{{ index .Data.data (file \"/etc/hostname\" | sprig_trim) }}{{ end }}"
  destination = "/etc/kevnet-backup/repo.pwd"
  perms = "0600"
  error_on_missing_key = true
  wait = "10s:30s"
}
