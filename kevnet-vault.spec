Name:		kevnet-vault
Version:	%{ci_version}
Release:	%{ci_release}%{?dist}
Summary:	KevNet Vault Integration

License:	Apache-2.0
URL:		https://gitlab.com/kev-net/rpms/kevnet-vault/
Source0:    kevnet-vault.sysconfig
Source1:    kevnet-vault.conf
Source2:    kevnet-vault.hcl
Source3:    knvs
Source4:    kevnet-vault-certs
Source5:    kevnet-vault-policy
Source6:    50-kevnet-vault.preset
Source7:    kevnet-vault-agent.service
Source8:    kevnet-vault-agent.sysconfig
Source9:    agent.hcl
Source10:   backup.hcl
Source11:   ssh.hcl
Source12:   50-vault.conf
Source13:   auto_auth.hcl

BuildArch:      noarch
BuildRequires:  systemd-rpm-macros, coreutils
Requires:       vault

%description
Integration for Vault into KevNet.

%package filesystem
Summary: Filesystem components required for kevnet-vault subpackages
Requires: %{name}

%description filesystem
The filesystem components required for the kevnet-vault subpackages.

%package server
Summary: Package for integrating Vault server
Requires: %{name}-filesystem, kevnet, kevnet-backup, systemd, crontabs, coreutils, awscli2, openssl, curl, grep, jq, git-core, findutils, gawk, sed, make, terraform

%description server
The components necessary to make the Vault server work in kevnet.

%package agent
Summary: Package for integrating Vault agent
Requires: %{name}-filesystem, bash, coreutils, grep, openssh-server

%description agent
The components necessary to make the Vault agent work in kevnet.


%install
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet-vault
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet-vault/agent.d
cp %{SOURCE9} %{buildroot}/%{_sysconfdir}/kevnet-vault/agent.d/agent.hcl
cp %{SOURCE13} %{buildroot}/%{_sysconfdir}/kevnet-vault/agent.d/auto_auth.hcl
cp %{SOURCE10} %{buildroot}/%{_sysconfdir}/kevnet-vault/agent.d/backup.hcl
cp %{SOURCE11} %{buildroot}/%{_sysconfdir}/kevnet-vault/agent.d/ssh.hcl
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet-vault/server.d
cp %{SOURCE2} %{buildroot}/%{_sysconfdir}/kevnet-vault/server.d/kevnet-vault.hcl
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet-vault/secrets
mkdir -p %{buildroot}/%{_sysconfdir}/sysconfig
cp %{SOURCE0} %{buildroot}/%{_sysconfdir}/sysconfig/kevnet-vault
cp %{SOURCE8} %{buildroot}/%{_sysconfdir}/sysconfig/kevnet-vault-agent
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet-backup
cp %{SOURCE12} %{buildroot}/%{_sysconfdir}/kevnet-backup/50-vault.conf
mkdir -p %{buildroot}/%{_unitdir}/vault.service.d
cp %{SOURCE1} %{buildroot}/%{_unitdir}/vault.service.d/kevnet-vault.conf
cp %{SOURCE7} %{buildroot}/%{_unitdir}/kevnet-vault-agent.service
mkdir -p %{buildroot}/%{_sbindir}
cp %{SOURCE3} %{buildroot}/%{_sbindir}/knvs
mkdir -p %{buildroot}/%{_sysconfdir}/cron.daily
mkdir -p %{buildroot}/%{_sysconfdir}/cron.hourly
cp %{SOURCE4} %{buildroot}/%{_sysconfdir}/cron.daily/kevnet-vault-certs
cp %{SOURCE5} %{buildroot}/%{_sysconfdir}/cron.hourly/kevnet-vault-policy
mkdir -p %{buildroot}/%{_presetdir}
cp %{SOURCE6} %{buildroot}/%{_presetdir}/50-kevnet-vault.preset


%files
%defattr(644,root,root)
%license LICENSE
%doc README.md


%files filesystem
%defattr(644,root,root)
%license LICENSE
%doc README.md
%dir %attr(755,vault,vault) %{_sysconfdir}/kevnet-vault
%attr(644,root,root) %{_presetdir}/50-kevnet-vault.preset


%files server
%defattr(644,vault,vault)
%license LICENSE
%doc README.md
%attr(700,vault,vault) %{_sysconfdir}/kevnet-vault/server.d
%attr(644,vault,vault) %{_sysconfdir}/kevnet-vault/server.d/kevnet-vault.hcl
%attr(700,root,root) %{_sysconfdir}/kevnet-vault/secrets
%config(noreplace) %attr(644,root,root) %{_sysconfdir}/sysconfig/kevnet-vault
%attr(755,root,root) %{_unitdir}/vault.service.d
%attr(644,root,root) %{_unitdir}/vault.service.d/kevnet-vault.conf
%attr(755,root,root) %{_sbindir}/knvs
%attr(755,root,root) %{_sysconfdir}/cron.daily/kevnet-vault-certs
%attr(755,root,root) %{_sysconfdir}/cron.hourly/kevnet-vault-policy
%attr(644,root,root) %{_sysconfdir}/kevnet-backup/50-vault.conf


%files agent
%defattr(644,vault,vault)
%license LICENSE
%doc README.md
%attr(755,vault,vault) %{_sysconfdir}/kevnet-vault/agent.d
%attr(644,vault,vault) %{_sysconfdir}/kevnet-vault/agent.d/agent.hcl
%attr(644,vault,vault) %{_sysconfdir}/kevnet-vault/agent.d/auto_auth.hcl
%attr(644,vault,vault) %{_sysconfdir}/kevnet-vault/agent.d/backup.hcl
%attr(644,vault,vault) %{_sysconfdir}/kevnet-vault/agent.d/ssh.hcl
%config(noreplace) %attr(644,root,root) %{_sysconfdir}/sysconfig/kevnet-vault-agent
%attr(644,root,root) %{_unitdir}/kevnet-vault-agent.service


%post agent
# Basic reloads
systemctl daemon-reload
systemctl try-restart kevnet-vault-agent


%preun agent
# Stop the agent
if [ "$1" -lt 1 ]; then
   systemctl stop kevnet-vault-agent
fi


%changelog
* Fri Oct 20 2023 Kevin L. Mitchell <klmitch@mit.edu>
- Adding the agent

* Fri Feb 17 2023 Kevin L. Mitchell <klmitch@mit.edu>
- Initial creation of the RPM
