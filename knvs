#!/bin/sh

# Directory containing the Vault database
vault_dir=/opt/vault
data_dir=/opt/vault/data

# Locations for secrets
sec_inst=/etc/kevnet-vault/secrets
sec_perm=/opt/vault/secrets
sec_open=/run/kevnet-vault/knvs-secrets

# Locations for certificates
cert_file=/etc/pki/tls/certs/vault.pem
key_file=/etc/pki/tls/private/vault.key
ca_file=/usr/share/pki/ca-trust-source/anchors/kevnet-ca.pem

# Subject for the vault certificate
subject="/C=US/ST=FL/L=Glen Saint Mary/O=KevNet/CN=vault.int.kevnet.net"

# Key size for the RSA key
key_size=4096

# Default number of days the certificate should be good for
cert_days=30

# Grace period before the cert will be rotated; this value is in
# seconds
cert_grace=604800 # 7 days

# Path to the kevnet public key; this will be used to encrypt the
# recovery key.
kevnet_pgp=/etc/pki/rpm-gpg/RPM-GPG-KEY-kevnet

# Path to store the recovery key in.  It will be in the init_data, but
# if the AWS key is lost, the recovery key will be necessary to
# recover the vault data.
recovery_path=/opt/vault/recovery/key.b64

# URL for the policy repo.
policy_repo=https://gitlab.com/kev-net/vault-policy.git

# Path for the policy repo.
policy_path=/opt/vault/vault-policy

# Permissions that should be applied to files or directories created
# by this script.  Except for temporary files, any file or directory
# not listed here that needs to be created would result in an error.
# (Created secrets files are an exception to this rule.)
declare -A permissions=(
    [/etc/kevnet-vault]=0755:vault:vault
    [/etc/kevnet-vault/secrets]=0700:root:root
    [/etc/pki/tls/certs/vault.pem]=0644:vault:vault
    [/etc/pki/tls/private/vault.key]=0600:vault:vault
    [/opt/vault]=0755:vault:vault
    [/opt/vault/data]=0755:vault:vault
    [/opt/vault/recovery]=0700:root:root
    [/opt/vault/recovery/key.b64]=0600:root:root
    [/opt/vault/secrets]=0700:root:root
    [/run/kevnet-vault]=0700:root:root
    [/run/kevnet-vault/knvs-secrets]=0700:root:root
)

# Because we handle sensitive information, set a restrictive umask for
# the script.
umask 0077

# log outputs logging information
log() {
    local level=$1
    shift
    local msg=$*

    # Set level for bare messages to "info"
    if [ "${msg}" == "" ]; then
        msg=${level}
        level=info
    fi

    # Handle level disposition
    if [ "${level@U}" == "DEBUG" ] && [ -z "${KNVS_DEBUG}" ]; then
        return 0
    elif [ "${level@U}" == "FATAL" ]; then
        level=error
        fatal=true
    fi

    echo "[${level@U}] ${msg}" >&2

    if [ "${fatal}" == true ]; then
        exit 1
    fi
}

# prepdir prepares a named directory.  The directory and its parents
# are created if necessary, and the correct permissions are applied.
# If the parent directory does not exist and is not listed in the
# permissions array, this is treated as a fatal error.
prepdir() {
    local target=$1

    # Do nothing if the directory exists
    if [ -d "${target}" ]; then
        return 0
    fi

    # Check if the directory is listed
    local perms=${permissions[${target}]}
    if [ -z "${perms}" ]; then
        log fatal "Cannot create directory \"${target}\": not declared"
    fi

    # Check if the parent exists and create it if needed
    # shellcheck disable=SC2155
    local parent=$(dirname "${target}")
    if [ ! -d "${parent}" ]; then
        prepdir "${parent}"
    fi

    # Extract the mode and user
    # shellcheck disable=SC2155
    local mode=$(echo "${perms}" | cut -d: -f1)
    # shellcheck disable=SC2155
    local owner=$(echo "${perms}" | cut -d: -f2-3)

    # Construct the directory
    log debug "Creating dir \"${target}\": mode \"${mode}\", owner \"${owner}\""
    mkdir -m "${mode}" "${target}"
    chown "${owner}" "${target}"
}

# shine prepares a created file (or directory).  The named file will
# have its permissions set according to what's declared in the
# permissions array, similar to how prepdir works.  The optional
# second argument provides a source filename; the file will be copied
# from that location.
shine() {
    local target=$1
    local source=$2

    # Check if the file is listed
    local perms=${permissions[${target}]}
    if [ -z "${perms}" ]; then
        log fatal "Cannot create file \"${target}\": not declared"
    fi

    # Extract the mode and user
    # shellcheck disable=SC2155
    local mode=$(echo "${perms}" | cut -d: -f1)
    # shellcheck disable=SC2155
    local owner=$(echo "${perms}" | cut -d: -f2-3)

    # Copy the file over, if requested
    if [ -n "${source}" ]; then
        log debug "Copying \"${target}\" from source \"${source}\""
        cp "${source}" "${target}"
    elif [ ! -e "${target}" ]; then
        log fatal "Cannot shine \"${target}\": does not exist"
    fi

    # Set the permissions and ownership
    log debug "Shining \"${target}\": mode \"${mode}\", owner \"${owner}\""
    chmod "${mode}" "${target}"
    chown "${owner}" "${target}"
}

# awsmeta retrieves a given piece of data from the AWS metadata
# server.  The data is specified by the path that follows the
# "meta-data" element of the URL.
awsmeta() {
    local datum=$1
    local -n var=$2

    # If the variable is set, use its value
    if [ -n "${var}" ]; then
        echo "${var}"
        return 0
    fi

    curl -m 5 -s "http://169.254.169.254/latest/meta-data/${datum}"
}

# Initialize secrets
init_secrets() {
    # Don't do anything if we're already initialized
    if [ -n "${key_id}" ]; then
        return 0
    fi

    # Load the key ID for secrets
    key_id=$(cat /etc/kevnet-vault/key-id)

    # Make sure all directories exist and have the correct permissions
    prepdir "${sec_inst}"
    prepdir "${sec_perm}"
    prepdir "${sec_open}"
}

# find_secret looks up and outputs the location of a secret file.
find_secret() {
    local fname=$1

    # Initialize the secrets subsystem
    init_secrets

    # Prefer permanent secrets to instance secrets
    if [ -e "${sec_perm}/${fname}.enc" ]; then
        echo "${sec_perm}/${fname}.enc"
    elif [ -e "${sec_inst}/${fname}.enc" ]; then
        echo "${sec_inst}/${fname}.enc"
    else
        return 1
    fi
}

# get_secret gets the path for the opened version of a secret,
# decrypting it on the fly if needed.  The output will be the path to
# the decrypted secret.
get_secret() {
    local fname=$1

    # Initialize the secrets subsystem
    init_secrets

    local opened="${sec_open}/${fname}"

    # Is it opened already?
    if [ ! -e "${opened}" ]; then
        # OK, locate the secret to open
        # shellcheck disable=SC2155
        local secret=$(find_secret "${fname}")
        if [ -z "${secret}" ]; then
            log fatal "Missing secret \"${fname}\""
        fi

        # Unlock it
        plain=$(aws kms decrypt --key-id "${key_id}" \
                    --ciphertext "file://${secret}" \
                    --query Plaintext --output text)
        if [ -z "${plain}" ]; then
            log fatal "Failed to decrypt secret \"${fname}\""
        fi

        # Decode and output it
        log debug "Writing secret \"${fname}\" to ${opened}"
        echo "${plain}" | base64 -d > "${opened}"
        chmod 0600 "${opened}"
    fi

    echo "${opened}"
}

# save_secret encrypts a secret, storing it as a permanent secret.
save_secret() {
    local fname=$1
    local secret=$2

    # Initialize the secrets subsystem
    init_secrets

    local opened="${sec_open}/${fname}"
    local crypted="${sec_perm}/${fname}.enc"

    # Write the secret to the opened file
    echo "${secret}" > "${opened}"
    chmod 0600 "${opened}"

    # Encrypt it
    cipher=$(aws kms encrypt --key-id "${key_id}" \
                 --plaintext "fileb://${opened}" \
                 --query CiphertextBlob --output text)
    if [ -z "${cipher}" ]; then
        log fatal "Failed to write secret \"${fname}\""
    fi

    # Save it
    log debug "Writing encrypted secret to \"${fname}\""
    echo "${cipher}" > "${crypted}"
    chmod 0600 "${crypted}"
}

# subject_alt_name constructs the alternate names that will be used
# when constructing the certificate
subject_alt_name() {
    local subj=$1

    # Need the canonical IP address
    # shellcheck disable=SC2155
    local canon_ip=$(awsmeta local-ipv4 AWS_INSTANCE_LOCAL_IP)

    # Capture the hostname from the subject
    # shellcheck disable=SC2155
    local cn=$(echo "${subj}" | tr / '\n' | grep '^CN=' | cut -d= -f2)

    # Construct the input for the subjectAltName
    echo "DNS:${cn},IP:${canon_ip},IP:127.0.0.1"
}

# construct_cert constructs a new certificate for the vault server.
construct_cert() {
    local subj=$1

    # Construct a temporary directory for the CSR
    # shellcheck disable=SC2155
    local tmpdir=$(mktemp -d)
    # shellcheck disable=SC2064
    trap "rm -rf \"${tmpdir}\"" EXIT

    # Construct the certificate request
    local csr="${tmpdir}/vault.csr"
    local key="${tmpdir}/vault.key"
    log debug "Constructing a new certificate request in ${csr}"
    openssl req -batch -out "${csr}" -new -newkey "rsa:${key_size}" \
            -keyout "${key}" -noenc -subj "${subj}"
    # shellcheck disable=SC2181
    if [ $? -ne 0 ] || [ ! -s "${csr}" ]; then
        log fatal "Failed to construct CSR"
    fi

    # Sign the request
    local crt="${tmpdir}/vault.pem"
    log debug "Signing the certificate request"
    openssl x509 -req -in "${csr}" -CA "${ca_file}" \
            -CAkey "$(get_secret ssl-ca.key)" -days "${cert_days}" \
            -out "${crt}"  -set_serial "$(date +%s)" \
            -extensions san \
            -extfile <(echo "[san]"; \
                       echo "subjectAltName=$(subject_alt_name "${subj}")")
    # shellcheck disable=SC2181
    if [ $? -ne 0 ] || [ ! -s "${crt}" ]; then
        log fatal "Failed to construct certificate"
    fi

    # Move the certificate material to where it's supposed to be
    log debug "Installing generated certificate"
    shine "${key_file}" "${key}"
    shine "${cert_file}" "${crt}"
}

# check_cert checks to see if the certificate needs to be rotated.
check_cert() {
    local grace=$1

    openssl x509 -in "${cert_file}" -noout -checkend "${grace}" > /dev/null
}

# prepare_cert checks that the certificates are valid, constructing
# new ones if needed.
prepare_cert() {
    local reload=$1

    # Do we need to construct cew certificates?
    if [ -s "${cert_file}" ] && check_cert "${cert_grace}"; then
        return 0
    fi

    # Construct new certificates
    construct_cert "${subject}"

    # Reload vault, if requested
    if [ -n "${reload}" ] && systemctl --quiet is-active vault; then
        systemctl reload vault
    fi
}

# init_vault_token sets up the vault token environment variable,
# drawing it from the initialization data.  This is normally handled
# by init_vault_client, but if the vault server needs initialization,
# it is done at a different step.
init_vault_token() {
    # shellcheck disable=SC2155
    export VAULT_TOKEN="$(jq -r '.root_token' < "$(get_secret init-data)")"
}

# init_vault_client initializes the vault client, readying it for
# interacting with the vault server.
init_vault_client() {
    # Don't do anything if we're already initialized
    if [ -n "${VAULT_ADDR}" ]; then
        return 0
    fi

    # Set the VAULT_ADDR environment variable
    # shellcheck disable=SC2155
    local cn=$(echo "${subject}" | tr / '\n' | grep '^CN=' | cut -d= -f2)
    export VAULT_ADDR="https://${cn}:8200"

    # Has vault been initialized?
    if find_secret init-data >/dev/null 2>&1; then
        init_vault_token
    fi
}

# init_vault initializes the vault server if necessary.  The security
# data emitted is stored as a secret, but the recovery key is also
# extracted and stored separately.
init_vault() {
    # Initialize the client
    init_vault_client

    # If we have the vault token available, then the server's been
    # initialized
    if [ -n "${VAULT_TOKEN}" ]; then
        return 0
    fi

    # Initialize the vault server, capturing the (compacted)
    # initialization data
    # shellcheck disable=SC2155
    local init_data=$(vault operator init -format json \
                            -recovery-pgp-keys "${kevnet_pgp}" \
                            -recovery-shares 1 \
                            -recovery-threshold 1 | jq -c)
    if [ -z "${init_data}" ]; then
        log fatal "Failed to initialize vault server"
    fi

    # Save the security data
    save_secret init-data "${init_data}"

    # Save the recovery key
    prepdir "$(dirname ${recovery_path})"
    echo "${init_data}" | jq -r '.recovery_keys_b64[0]' > "${recovery_path}"
    shine "${recovery_path}"

    # Set the vault token
    init_vault_token
}

# cred_helper implements the git credential helper protocol,
# interpreting the policy-token secret and outputting the correct
# data.
cred_helper() {
    # Only handle get operations
    if [ "$1" != "get" ]; then
        return 0
    fi

    # Get the policy token data
    # shellcheck disable=SC2155
    local token=$(cat "$(get_secret policy-token)")

    # Extract the username and the password
    # shellcheck disable=SC2155
    local username=$(echo "${token}" | cut -d: -f1)
    # shellcheck disable=SC2155
    local password=$(echo "${token}" | cut -d: -f2-)

    echo "username=${username}"
    echo "password=${password}"
}

# init_policy clones the policy repository into the correct location,
# or, if already cloned, pulls the most recent updates.
init_policy() {
    # Only act if the policy repository isn't present
    if [ -d "${policy_path}" ]; then
        log debug "Pulling changes to the policy repository"
        git -c "credential.helper=/usr/sbin/knvs cred-helper" \
            -C "${policy_path}" pull >/dev/null 2>&1
        return $?
    fi

    # Clone the repository
    log debug "Cloning the policy repository"
    git -c "credential.helper=/usr/sbin/knvs cred-helper" \
        clone "${policy_repo}" "${policy_path}" >/dev/null 2>&1
}

# apply_policy applies the policy specified in the policy repository.
apply_policy() {
    init_vault_client
    make -C "${policy_path}" clean apply
}

# prepare prepares for starting the vault server.
prepare() {
    log debug "Prepare called"

    # Prepare the vault directories; we shine the vault dir to make
    # sure it has the correct permissions, since it could be a mount
    shine "${vault_dir}"
    prepdir "${data_dir}"

    # Make sure a certificate exists
    prepare_cert
}

# init checks if the vault server requires initialization, performing
# it if it is needed.
init() {
    log debug "Init called"

    # Make sure that the vault server is initialized
    init_vault

    # Initialize the policy repository and apply it
    init_policy || log fatal "Failed to initialize policy repository"
    apply_policy || log fatal "Failed to apply policy repository"
}

# certs checks and prepares the certificate for the vault server,
# constructing a new one if required.
certs() {
    log debug "Certs called"
    prepare_cert reload
}

# policy checks for updates regarding the vault server policy,
# applying them as necessary.
policy() {
    log debug "Policy called"

    # Initialize the policy repository and apply it
    init_policy || log fatal "Failed to initialize policy repository"
    apply_policy || log fatal "Failed to apply policy repository"
}

case "$1" in
    find-secret)
        find_secret "$2";;
    get-secret)
        get_secret "$2";;
    save-secret)
        save_secret "$2" "$3";;
    construct-cert)
        construct_cert "${subject}";;
    cred-helper)
        cred_helper "$2";;
    vault)
        init_vault_client
        shift
        exec vault "$@";;

    prepare)
        prepare;;
    init)
        init;;
    certs)
        certs;;
    policy)
        policy;;
    *)
        echo "Unknown operation \"$1\"" >&2
        exit 1;;
esac
