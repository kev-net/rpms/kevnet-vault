ui       = true

storage "file" {
  path = "/opt/vault/data"
}

listener "tcp" {
  address       = "0.0.0.0:8200"
  tls_cert_file = "/etc/pki/tls/certs/vault.pem"
  tls_key_file  = "/etc/pki/tls/private/vault.key"
}
