template {
  contents = <<EOT
{{ range $key := (plugin "sh" "-c" "ls /etc/ssh/ssh_host_* | grep -v '\\.pub$'" | split "\n") -}}
{{ $pub := printf "%s.pub" $key -}}
{{ $cert := printf "%s-cert.pub" $key -}}
{{ with secret "ssh_host/sign/ssh_hostrole" "cert_type=host" (printf "public_key=%s" (file $pub)) }}{{ .Data.signed_key | writeToFile $cert "" "" "0644" }}{{ end -}}
HostCertificate {{ $cert }}
{{ end -}}
EOT
  destination = "/etc/ssh/sshd_config.d/60-kevnet-certs.conf"
  perms = "0644"
  error_on_missing_key = true
  wait = "10s:30s"

  exec {
    command = ["systemctl", "try-restart", "sshd"]
  }
}
